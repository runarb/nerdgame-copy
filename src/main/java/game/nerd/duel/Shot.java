package game.nerd.duel;

import game.nerd.duel.players.Player;

public class Shot {
	Tile target;
	Player shooter;
	int powerLeft;
	int tilesTraveled;
	
	public Shot(Tile target, Player shooter)
	{
		this.target = target;
		this.shooter = shooter;
		tilesTraveled = 0;
		powerLeft = shooter.getFirepower();
	}

	public Tile getTarget() {
		return target;
	}

	public Player getShooter() {
		return shooter;
	}

	public int getPowerLeft() {
		return powerLeft;
	}

	public int getTilesTraveled() {
		return tilesTraveled;
	}
	
	
}
