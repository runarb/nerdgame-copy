package game.nerd.duel;

import game.nerd.duel.players.Player;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;

public class PlayerFlowPane extends FlowPane {
	Player player;
	Label score;
	Label sightRange;
	Label health;
	Label fireRange;
	Label firepower;
	Label moveSpeed;
	Label generator;
	Label armour;
	Label power;
	Label bonus;

	public PlayerFlowPane(Player player) {
		this.player = player;
		Label playerLabel = new Label("Player : " + player.getPlayerId());
		Label scoreLabel = new Label("Score");
		score = new Label(Integer.toString(player.getScore()));
		Label sightRangeLabel = new Label("Sight range"); // How far player can see
		sightRange = new Label(Integer.toString(player.getSightRange()));
		Label healthLabel = new Label("Health"); // How much health player has
		health = new Label(Integer.toString(player.getHealth()));
		Label fireRangeLabel = new Label("Fire range"); // How far player can fire
		fireRange = new Label(Integer.toString(player.getFireRange()));
		Label firepowerLabel = new Label("Fire power"); // How much damage player does at range 1
		firepower = new Label(Integer.toString(player.getFirepower()));
		Label moveSpeedLabel = new Label("Move speed"); // How many steps player can move per step
		moveSpeed = new Label(Integer.toString(player.getMoveSpeed()));
		Label generatorLabel = new Label("Generator"); // How much power player generates per turn.
		generator = new Label(Integer.toString(player.getGenerator()));
		Label armourLabel = new Label("Armour"); // How much armour player has to reduce damage
		armour = new Label(Integer.toString(player.getArmour()));
		Label powerLabel = new Label("Power"); // Current power
		power = new Label(Integer.toString(player.getPower()));
		Label bonusLabel = new Label("Bonus"); // How much bonus the player has to spend
		bonus = new Label(Integer.toString(player.getBonus()));
		
		getChildren().add(playerLabel);
		addAttributeLabels(scoreLabel, score);
		addAttributeLabels(sightRangeLabel, sightRange);
		addAttributeLabels(healthLabel, health);
		addAttributeLabels(fireRangeLabel, fireRange);
		addAttributeLabels(firepowerLabel, firepower);
		addAttributeLabels(moveSpeedLabel, moveSpeed);
		addAttributeLabels(generatorLabel, generator);
		addAttributeLabels(armourLabel, armour);
		addAttributeLabels(powerLabel, power);
		addAttributeLabels(bonusLabel, bonus);
		this.setVgap(5);
		this.setHgap(5);
	}

	private void addAttributeLabels(Label label, Label value) {
		getChildren().add(label);
		getChildren().add(value);
	}

	public void update() {
		score.setText(Integer.toString(player.getScore()));		
		sightRange.setText(Integer.toString(player.getSightRange()));
		health.setText(Integer.toString(player.getHealth()));
		fireRange.setText(Integer.toString(player.getFireRange()));
		firepower.setText(Integer.toString(player.getFirepower()));
		moveSpeed.setText(Integer.toString(player.getMoveSpeed()));
		generator.setText(Integer.toString(player.getGenerator()));
		armour.setText(Integer.toString(player.getArmour()));
		power.setText(Integer.toString(player.getPower()));
		bonus.setText(Integer.toString(player.getBonus()));
	}
}
