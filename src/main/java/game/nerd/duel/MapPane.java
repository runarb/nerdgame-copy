package game.nerd.duel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Random;
import java.util.function.Predicate;

import game.nerd.Util;
import game.nerd.duel.players.Player;
import javafx.geometry.Pos;
import javafx.scene.layout.TilePane;

public class MapPane extends TilePane {
	int columns;
	int rows;
	Tile[][] map;
	List<String> log = new ArrayList<>();
	Random random = new Random();

	public MapPane(int columns, int rows) {
		super();
		this.columns = columns;
		this.rows = rows;
		setCenterShape(true);
		setPrefColumns(rows);
		setPrefRows(rows);
		setAlignment(Pos.CENTER);
		setHgap(1);
		setVgap(1);
		map = new Tile[columns][rows];
		addTiles();
		paint();
	}

	public void paint() {
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				map[i][j].paint();
			}
		}
	}

	private void addTiles() {
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				Tile tile = new Tile(i, j);
				map[i][j] = tile;
				getChildren().add(tile);
			}
		}

	}

	public void addBlockers(int numberOfBlockers) {
		int placedBlockers = 0;

		while (placedBlockers < numberOfBlockers) {
			int column = random.nextInt(500) % columns;
			int row = random.nextInt(500) % rows;
			Tile tile = map[column][row];

			if (tile.isEmpty()) {
				tile.setBlocker(true);
				placedBlockers = placedBlockers + 1;
			}
		}
	}

	public void clear() {
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				map[i][j].setBlocker(false);
				map[i][j].setPlayer(null);
				map[i][j].setBonus(false);
			}
		}
	}

	/**
	 * Set player to a tile not having a blocker
	 */
	public void setPlayers(Collection<Player> players) {
		clearPlayer();

		for (Player player : players) {
			placePlayer(player);
		}
	}

	private Tile placePlayer(Player player) {
		boolean playerNotSet = true;
		Tile retVal = null;

		while (playerNotSet) {
			int column = random.nextInt(500) % columns;
			int row = random.nextInt(500) % rows;
			Tile tile = map[column][row];

			if (tile.isEmpty()) {
				tile.setPlayer(player);
				player.setTile(tile);
				playerNotSet = false;
				retVal = tile;
			}
		}
		return retVal;
	}

	private void clearPlayer() {
		for (int i = 0; i < columns; i++) {
			for (int j = 0; j < rows; j++) {
				map[i][j].setPlayer(null);
			}
		}

	}

	public void addBonus(int numberOfBonus) {
		int placedBonus = 0;

		while (placedBonus < numberOfBonus) {
			int column = random.nextInt(500) % columns;
			int row = random.nextInt(500) % rows;
			Tile tile = map[column][row];

			if (tile.isEmpty()) {
				tile.setBonus(true);
				placedBonus = placedBonus + 1;
			}
		}
	}

	public Tile[][] getMap() {
		return map;
	}

	/**
	 * @param fromTile
	 * @param toTile
	 * @return Distance between two tiles
	 */
	public int distance(Tile fromTile, Tile toTile) {
		int fromColumn = fromTile.getColumn();
		int fromRow = fromTile.getRow();
		int toColumn = toTile.getColumn();
		int toRow = toTile.getRow();
		return Util.distance(fromColumn, fromRow, toColumn, toRow);
	}

	/**
	 * @param column
	 * @param row
	 * @return Tile from map if column and row are values within the map. Else null.
	 */
	public Tile getTile(int column, int row) {
		if (column < columns && row < rows && column > -1 && row > -1) {
			return map[column][row];
		}
		return null;
	}

	/**
	 * @param player
	 * @return A shallow copy of a part of the map the size of the players sight
	 *         range
	 */
	public VoTile[][] look(Player player) {
		Tile playerTile = player.getTile();
		int sightRange = player.getSightRange();

		int fromColumn = Math.max(0, playerTile.getColumn() - sightRange);
		int toColumn = Math.min(columns - 1, playerTile.getColumn() + sightRange);
		int fromRow = Math.max(0, playerTile.getRow() - sightRange);
		int toRow = Math.min(rows - 1, playerTile.getRow() + sightRange);
		return copyMap(fromColumn, toColumn, fromRow, toRow);
	}

	/**
	 * Copy is shallow using VoTile to not reveal details to the implementing player
	 * classes
	 * 
	 * @param fromColumn
	 * @param toColumn
	 * @param fromRow
	 * @param toRow
	 * @return Shallow copy of the map for the given coordinates
	 */
	private VoTile[][] copyMap(int fromColumn, int toColumn, int fromRow, int toRow) {
		VoTile[][] retVal = new VoTile[(toColumn - fromColumn) + 1][(toRow - fromRow) + 1];
		int a = 0;

		for (int i = fromColumn; i <= toColumn; i++) {
			int b = 0;

			for (int j = fromRow; j <= toRow; j++) {
				retVal[a][b] = new VoTile(map[i][j], a, b);
				b = b + 1;
			}
			a = a + 1;
		}
		return retVal;
	}

	/**
	 * Make the shot fly
	 * 
	 * @param shot
	 */
	public void fire(Shot shot) {
		Player shooter = shot.getShooter();
		Tile startTile = shooter.getTile();
		Tile targetTile = shot.getTarget();
		log("Shot is fired by player " + shot.getShooter().getPlayerId() + " at player "
				+ targetTile.getPlayer().getPlayerId() + ". ");

		if (startTile != null) {
			int fromColumn = startTile.getColumn();
			int fromRow = startTile.getRow();
			int toColumn = targetTile.getColumn();
			int toRow = targetTile.getRow();
			Tile shotTile = canShootAtTile(fromColumn, fromRow, toColumn, toRow);

			if (shotTile != null && shotTile.tileEquals(targetTile) && shotTile.getPlayer() != null) {
				Player shotPlayer = shotTile.getPlayer();
				int damage = shotPlayer.isHit(shot);
				log("Player " + shotPlayer.getPlayerId() + " suffers " + damage + " damage. ");
			}
		}
	}

	public Tile canShootAtTile(int fromColumn, int fromRow, int toColumn, int toRow) {
		int stepColumn = Util.stepFactor(fromColumn, toColumn);
		int stepRow = Util.stepFactor(fromRow, toRow);
		Tile shotTile = stepStraight(fromColumn, stepColumn, fromRow, stepRow, Tile::hasBlocker);

		while (shotTile != null && (shotTile.getColumn() != toColumn || shotTile.getRow() != toRow)) {
			shotTile.strike();
			fromColumn = shotTile.getColumn();
			fromRow = shotTile.getRow();
			stepColumn = Util.stepFactor(fromColumn, toColumn);
			stepRow = Util.stepFactor(fromRow, toRow);
			shotTile = stepStraight(fromColumn, stepColumn, fromRow, stepRow, Tile::hasBlocker);
		}
		return shotTile;
	}

	private Tile stepStraight(int fromColumn, int stepColumn, int fromRow, int stepRow, Predicate<Tile> predicate) {
		Tile retVal = getTile(fromColumn + stepColumn, fromRow + stepRow);

		// If not legal value, try moving only one direction
		if (predicate.test(retVal)) {
			if (stepColumn != 0) {
				retVal = getTile(fromColumn + stepColumn, fromRow);

				if (retVal != null && predicate.test(retVal)) {
					retVal = null;
				}
			}

			if (stepRow != 0) {
				retVal = getTile(fromColumn, fromRow + stepRow);

				if (retVal != null && predicate.test(retVal)) {
					retVal = null;
				}
			}
		}
		return retVal;
	}

	public void log(String textToLog) {
		log.add(textToLog);
	}

	public List<String> getAndClearLog() {
		List<String> retVal = log;
		log = new ArrayList<>();
		return retVal;
	}
}
