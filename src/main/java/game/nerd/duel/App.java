package game.nerd.duel;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import game.nerd.duel.players.Player;
import game.nerd.duel.players.PlayerOne;
import game.nerd.duel.players.PlayerTwo;

/**
 * Game application for letting two AI controlled players slug it out
 */
public class App extends Application {

	protected static final int ADD_BLOCKS = 12;
	private static Scene scene;
	private static final int COLUMNS = 30;
	private static final int ROWS = 30;
	private int maxTurns = 1000;
	private int startingBonus = 50;
	private int bonusAddedTurn = 150;
	MapPane mapPane;
	FlowPane logPane;
	ScrollPane logScroller;
	int turnCounter = 0;
	int scoreCounter = 0;
	int bonusCounter = 0;
	final List<Player> playerList = new ArrayList<>();
	final List<PlayerFlowPane> playerFlowPaneList = new ArrayList<>();
	Label turn;

	private static Parent loadFXML(String fxml) throws IOException {
		FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource(fxml + ".fxml"));
		return fxmlLoader.load();
	}

	static void setRoot(String fxml) throws IOException {
		scene.setRoot(loadFXML(fxml));
	}

	@Override
	public void start(Stage primaryStage) {
		VBox root = new VBox();
		root.setFillWidth(false);
		FlowPane infoPane = new FlowPane();
		FlowPane turnPane = new FlowPane();
		logPane = new FlowPane();
		logScroller = new ScrollPane();
		logScroller.setContent(logPane);
		logScroller.setPrefSize(800, 200);

		infoPane.getChildren().add(turnPane);
		Label turnLabel = new Label("Turn");
		turn = new Label(Integer.toString(turnCounter));

		infoPane.getChildren().add(turnLabel);
		infoPane.getChildren().add(turn);
		infoPane.setVgap(5);
		infoPane.setHgap(5);

		mapPane = new MapPane(COLUMNS, ROWS);

		setupPlayers();

		FlowPane playerListPane = new FlowPane();
		playerListPane.setVgap(5);
		playerListPane.setHgap(5);
		setUpPlayerDataDisplay(playerListPane);
		infoPane.getChildren().add(playerListPane);

		FlowPane buttonPane = new FlowPane();
		buttonPane.getChildren().addAll(makeActionButtons());
		primaryStage.setTitle("Nerd duel");

		root.getChildren().add(infoPane);
		root.getChildren().add(mapPane);
		root.getChildren().add(buttonPane);
		root.getChildren().add(logScroller);

		primaryStage.setScene(new Scene(root, 900, 1150));
		primaryStage.show();
	}

	private void setUpPlayerDataDisplay(FlowPane playerListPane) {
		for (Player player : playerList) {
			PlayerFlowPane playerPane = new PlayerFlowPane(player);
			playerListPane.getChildren().add(playerPane);
			playerFlowPaneList.add(playerPane);
		}
	}

	/**
	 * @return The buttons to control the application
	 */
	private List<Button> makeActionButtons() {
		List<Button> buttonList = new ArrayList<>();
		Button addBlockersButton = new Button();
		addBlockersButton.setText("Add blockers");
		addBlockersButton.setOnAction(event -> handleAddBlocers());

		Button clearButton = new Button();
		clearButton.setText("Clear");
		clearButton.setOnAction(event -> handleClear());

		Button setPlayerButton = new Button();
		setPlayerButton.setText("Set players");
		setPlayerButton.setOnAction(event -> handleSetPlayers());

		Button addBonusButton = new Button();
		addBonusButton.setText("Add bonus");
		addBonusButton.setOnAction(event -> handleAddBonus());

		Button startButton = new Button();
		startButton.setText("Start");
		startButton.setOnAction(event -> handleStart());

		buttonList.add(addBlockersButton);
		buttonList.add(addBonusButton);
		buttonList.add(setPlayerButton);
		buttonList.add(startButton);
		buttonList.add(clearButton);
		return buttonList;
	}

	private void handleStart() {

		Task<Integer> task = new Task<Integer>() {
			@Override
			public Integer call() {
				runPlayers();
				return null;
			}
		};

		task.getWorkDone();
		Thread th = new Thread(task);
		th.setDaemon(true);
		th.start();
	}

	private void handleAddBonus() {
		addBonus();
	}

	private void handleSetPlayers() {
		mapPane.setPlayers(playerList);
		mapPane.paint();
	}

	private void handleClear() {
		clear();
		mapPane.paint();
	}

	private void handleAddBlocers() {
		mapPane.addBlockers(ADD_BLOCKS);
		mapPane.paint();
	}

	private void addBonus() {
		mapPane.addBonus(ADD_BLOCKS);
		bonusCounter = bonusCounter + ADD_BLOCKS;
		mapPane.paint();
	}

	private void clear() {
		turnCounter = 0;
		scoreCounter = 0;
		bonusCounter = 0;
		mapPane.clear();
		playerList.clear();
		setupPlayers();
	}

	/**
	 * Add two players using the default player AIs
	 */
	private void setupPlayers() {
		Player playerOne = new PlayerOne(1, startingBonus);
		playerOne.setMapPane(mapPane);
		playerList.add(playerOne);

		Player playerTwo = new PlayerTwo(2, startingBonus);
		playerTwo.setMapPane(mapPane);
		playerList.add(playerTwo);
	}

	public static void main(String[] args) {
		launch();
	}

	private Integer runPlayers() {
		boolean playersAllive = true;

		for (int i = 0; i < maxTurns && playersAllive; i++) {
			for (Player player : playerList) {
				playersAllive = movePlayer(player, playersAllive);
			}

			// Sometimes we give some more bonus tiles
			if (i % bonusAddedTurn == 0) {
				addBonus();
			}
			turnCounter = turnCounter + 1;
		}

		if (!playersAllive) {
			for (Player player : playerList)
				if (player.getHealth() <= 0) {
					mapPane.log("Game over. Player " + player.getPlayerId() + " is dead! ");
					Platform.runLater(() -> updateProgress(turnCounter));
				}
		}
		return turnCounter;
	}

	private boolean movePlayer(Player player, boolean playersAllive) {
		if (player.getHealth() > 0) {
			player.move();
		}

		for (Player playerInner : playerList) {
			playersAllive = playersAllive && playerInner.getHealth() > 0;
		}

		mapPane.paint();

		Platform.runLater(() -> updateProgress(turnCounter));

		// Wait slightly to make it possible to notice the map update
		try {
			Thread.sleep(50);
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		return playersAllive;
	}

	/**
	 * Update the information panels with updated data from the players and log
	 * 
	 * @param turnCounter
	 */
	private void updateProgress(int turnCounter) {

		for (PlayerFlowPane playerFlowPane : playerFlowPaneList) {
			playerFlowPane.update();
		}
		turn.setText(Integer.toString(turnCounter));

		List<String> logList = mapPane.getAndClearLog();

		for (String logText : logList) {
			Text text = new Text(logText);
			logPane.getChildren().add(text);
			logScroller.setVvalue(1.0);
		}
	}
}